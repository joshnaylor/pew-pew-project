﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UI : MonoBehaviour {

	public Text score;


	public static float playerHealth = 10;
	public static  int playerScore = 0000;
	public Image health;
	float h = 10;
	public GameObject pauseMenu;

	public static int playerLives = 1;
	public RectTransform sliderHandle;
	public Slider healthSlider;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		score.text = playerScore.ToString();

		health.fillAmount = playerHealth / h;
		if(health.fillAmount < 0.6 && health.fillAmount > 0.3)
		{
			health.color = Color.yellow;
		}
		else if(health.fillAmount < 0.3)
		{
			health.color = Color.red;
		}
		else
		{
			health.color = Color.green;
		}
		sliderHandle.localScale = new Vector3 (playerHealth/10, 1, 1);
		healthSlider.value = playerHealth / h;

	}

	public static void minusLive()
	{
		playerLives -= 1;
	}

	public static void addScore(int score)
	{
		playerScore += score;
	}

	public static void minusHealth(int damage)
	{
		playerHealth -= damage;
	}

	public void pause()
	{
		Time.timeScale = 0;
		pauseMenu.SetActive (true);
	}

	public void unPause()
	{
		Time.timeScale = 1;
		pauseMenu.SetActive (false);
	}
}
