﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Analytics;

public class analytics : MonoBehaviour {

	void Start()
	{
//		Analytics.CustomEvent ("testEvent", new Dictionary<string, object>
//        {
//			{ "name", "josh" },
//			{ "fav colour", "red"}
//		});
		TransactionInGame();
		AnalyticsUserInfo ();
	}

	void OnEnable()
	{
		string spriteName= gameObject.GetComponent<SpriteRenderer> ().sprite.name;

		Debug.Log ("enabled");
		Debug.Log ("ship name = " + spriteName);
		CallCustomEvent("shipChoice", spriteName);
	}

	public void CallCustomEvent(string nameOfEvent, string value)
	{
		if(nameOfEvent == "shipChoice")
		{
			Debug.Log("call custom event");

			Analytics.CustomEvent ("Useful to our game", new Dictionary<string, object>
            {
				{ "Choice of Ship", value }
			});

		}
		if(nameOfEvent == "dead")
		{
			Debug.Log("dead");
			int score = UI.playerScore;

			Analytics.CustomEvent ("Player Died", new Dictionary<string, object>
            {
				{ "score", score },
				{ "bullets used", playerControl.shoot},
				{ "killed by", value}
			});

		}
	}

	void TransactionInGame()
	{
		//Analytics.Transaction(string productId, decimal price, string currency, string receipt, string signature);
		float price = Random.Range (0.0f, 0.99f);
		decimal value = (decimal)price;

		Analytics.Transaction("people buying ships", value, "USD", null, null);

	}

	void AnalyticsUserInfo()
	{
		Gender gender = Gender.Female;
		Analytics.SetUserGender(gender);
		
		int birthYear = Random.Range(1950, 2015);
		Analytics.SetUserBirthYear(birthYear);
	}
}
