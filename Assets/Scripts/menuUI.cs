﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class menuUI : MonoBehaviour {

	public GameObject storyUI;
	public GameObject player;
	public GameObject gameUI;
	Animator anim;
	bool shipPanel = true;
	bool leaderboardPanel = false;
	bool audioPanel = false;

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> ();
		Time.timeScale = 0;

		gameUI.SetActive (false);
		player.SetActive (false);
		storyUI.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
	}

	public void LoadGame()
	{
		//Application.LoadLevel(levelName);
		Time.timeScale = 1;
		storyUI.SetActive (false);
		gameUI.SetActive (true);
		player.SetActive (true);
	}

	public void LoadStory()
	{
		gameObject.SetActive (false);
		storyUI.SetActive (true);
	}

	public void shipPanelSelect()
	{
		if(!shipPanel)
		{
			anim.SetTrigger("ship");
			shipPanel = true;
			leaderboardPanel = false;
			audioPanel = false;
		}
	}

	public void leaderboardPanelSelect()
	{
		if(!leaderboardPanel)
		{
			anim.SetTrigger("leaderboard");
			leaderboardPanel = true;
			shipPanel = false;
			audioPanel = false;
		}
	}

	public void audioPanelSelect()
	{
		if(!shipPanel)
		{
			anim.SetTrigger("ship");
			audioPanel = true;
			leaderboardPanel = false;
			shipPanel = false;
		}
	}
}
