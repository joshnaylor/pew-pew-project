﻿using UnityEngine;
using System.Collections;


public class playerControl : MonoBehaviour {

	public GameObject leftTurn;
	public GameObject rightTUrn;
	public GameObject firePoint;
	public GameObject laser;
	SpriteRenderer shipREnderer;
	public static int shoot = 0;
	public float speed = 10;
	float x;
	// Use this for initialization
	void Start () {
	

	}
	
	// Update is called once per frame
	void Update () {
	

#if UNITY_ANDROID

		x = Input.acceleration.x * speed;

#endif

		x = Input.GetAxis("Horizontal") * speed;	
		//For Snappy Left 2 Right
		//rigidbody2D.velocity = new Vector2 (x, 0) * speed;

		//For delayed 
		GetComponent<Rigidbody2D>().AddForce(new Vector2(x,0));

		if (x < -0.5)
		{
			leftTurn.SetActive(true);
			rightTUrn.SetActive(false);
		}
		else if (x > 0.5)
		{
			leftTurn.SetActive(false);
			rightTUrn.SetActive(true);
		}
		else
		{
			leftTurn.SetActive(false);
			rightTUrn.SetActive(false);
		}

		if (Input.GetMouseButtonDown (0)) 
		{
			Shoot();
			Debug.Log("Shoot");
		}
	}

	void Shoot()
	{
		shoot++;
		Instantiate (laser, firePoint.transform.position, transform.rotation);
	}
}
