﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class shipSelect : MonoBehaviour {

	public GameObject player;
	SpriteRenderer playerSprite;
	public Sprite[] ships;
	Image shipImage;
	public int currentShip = 0;
	// Use this for initialization
	void Start () {
		playerSprite = player.GetComponent<SpriteRenderer> ();
		shipImage = GetComponent<Image> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void nextShip(string direction)
	{
		if(direction == "forward")
		{
			currentShip ++;
			if(currentShip == ships.Length)
				currentShip = 0;

			shipImage.overrideSprite = ships[currentShip];
			playerSprite.sprite = ships[currentShip];
			//PlayerPrefs.SetInt("Ship Choice", currentShip);
			Debug.Log(currentShip);
		}
		else
		{
			currentShip --;
			if(currentShip == -1)
				currentShip = (ships.Length - 1);
			
			shipImage.overrideSprite = ships[currentShip];
			playerSprite.sprite = ships[currentShip];
			//PlayerPrefs.SetInt("Ship Choice", currentShip);
		}
	}
}
