﻿using UnityEngine;
using System.Collections;

public class bullet : MonoBehaviour {

	public GameObject smallRocks; 
	public Vector2 bulletSpeed = new Vector2(0,2);
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		GetComponent<Rigidbody2D>().velocity = bulletSpeed;
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.GetComponent<Rigidbody2D>().tag == "UFO") 
		{
			Destroy(other.gameObject);
			Destroy(gameObject);
			UI.addScore(5);
		}

		if(other.GetComponent<Rigidbody2D>().tag == "Player")
		{
			Destroy(gameObject);
			UI.minusHealth(2);
			if (UI.playerHealth <= 0)
			{
				UI.minusLive();
				UI.playerHealth = 10;
			}
			if (UI.playerLives < 1)
			{
				//Destroy(other.gameObject);
				//other.GetComponentInParent<analytics>().CallCustomEvent("dead", "bullet");
				//other.GetComponentInParent<ads>().ShowAd();
				//Debug.Log("dead event called");

			}
		}

		if(other.GetComponent<Rigidbody2D>().name == "meteorBrown_big3(Clone)")
		{
			if(gameObject.name == "laserRed02(Clone)")
			{
				Destroy(gameObject);
				Destroy(other.gameObject);
				Instantiate(smallRocks, transform.position, transform.rotation);
				Instantiate(smallRocks, transform.position, transform.rotation);
				Instantiate(smallRocks, transform.position, transform.rotation);
				UI.addScore(2);
			}
		}

		if(other.GetComponent<Rigidbody2D>().name == "meteorBrown_med3(Clone)")
		{
			if(gameObject.name == "laserRed02(Clone)")
			{
				Destroy(gameObject);
				Destroy(other.gameObject);
				UI.addScore(1);
			}
		}

		if(other.gameObject.tag == "Bounds")
		{
			Destroy(gameObject);
		}
	}
}
