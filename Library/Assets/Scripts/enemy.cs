﻿using UnityEngine;
using System.Collections;

public class enemy : MonoBehaviour {

	public Vector2 enemyMovement = new Vector2(0,-2);
	public float enemySpeed = 2;
	public GameObject laser;
	public GameObject firePoint;
	public float rateOfFire = 1;

	// Use this for initialization
	void Start () {

		InvokeRepeating ("fire", 0.5f, rateOfFire);
	}
	
	// Update is called once per frame
	void Update () {

		GetComponent<Rigidbody2D>().velocity = enemyMovement * enemySpeed;
	}

	void fire()
	{
		Instantiate (laser, firePoint.transform.position, transform.rotation);
	}
}
