﻿using UnityEngine;
using System.Collections;

public class rocks : MonoBehaviour {

	public Vector2 rockMovement = new Vector2(0,-2);
	public float rockSpeed = 2;
	// Use this for initialization
	void Start () {
		rockMovement = new Vector2 (Random.Range (-2, 2), Random.Range (0, -2));
		rockSpeed = Random.Range (1, 4);
	}
	
	// Update is called once per frame
	void Update () {
		GetComponent<Rigidbody2D>().velocity = rockMovement * rockSpeed;
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if(other.gameObject.tag == "Player")
		{
			Destroy(gameObject);
			UI.minusHealth(1);
			if (UI.playerHealth < 0)
			{
				UI.minusLive();
				UI.playerHealth = 10;
			}
			if (UI.playerLives < 1)
			{
				//Destroy(other.gameObject);
				//other.GetComponentInParent<analytics>().CallCustomEvent("dead", "bullet");
				//other.GetComponentInParent<ads>().ShowAd();
				//Debug.Log("dead event called");

			}
		}

		if(other.gameObject.tag == "Bounds")
		{
			Destroy(gameObject);
		}
	}
}
