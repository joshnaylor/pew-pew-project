﻿using UnityEngine;
using System.Collections;

public class spawnRocks : MonoBehaviour {

	public GameObject rock1;
	public GameObject rock2;
	public float spawnerRate = 3;
	// Use this for initialization
	void Start () {
		InvokeRepeating ("spawn", 1, spawnerRate);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void spawn()
	{
		int rockType = Random.Range (0, 2);
		Debug.Log (rockType);
		GameObject rock;
		if (rockType == 0) 
		{
			rock = rock1;
		}
		else
		{
			rock = rock2;
		}
		float x = Random.Range (-7, 7);
		Vector3 rockPos = new Vector3 (x, transform.position.y, transform.position.z);
		Instantiate (rock, rockPos, transform.rotation);
	}
}
