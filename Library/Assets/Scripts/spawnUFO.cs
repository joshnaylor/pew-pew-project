﻿using UnityEngine;
using System.Collections;

public class spawnUFO : MonoBehaviour {

	public GameObject UFO;
	public float spawnerRate = 3;
	// Use this for initialization
	void Start () {
		InvokeRepeating ("spawn", 1, spawnerRate);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void spawn()
	{
		float x = Random.Range (-7, 7);
		Vector3 ufoPos = new Vector3 (x, transform.position.y, transform.position.z);
		Instantiate (UFO, ufoPos, transform.rotation);
	}
}
